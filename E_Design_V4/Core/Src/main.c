/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* Includes ------------------------------------------------------------------*/
#include "sinewave.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
extern volatile uint8_t interupt;
uint8_t myStudentNumber[10]= {127,128, '2', '0', '7', '8', '2', '0', '2', '0'};
uint8_t Record[10]= {127,128, 'R', 'e', 'c', 'o', 'r', 'd', '_'};
uint8_t Play[10]= {127,128, 'P', 'l', 'a', 'y', '_', '_', '_'};
uint8_t Stop[10]= {127,128, 'S', 't', 'o', 'p', '_', '_', '_', '_'};
uint8_t but_1 = 0;
uint8_t but_2 = 0;
uint8_t but_3 = 0;
uint8_t but_stop = 0;
uint8_t but_rec = 0;
uint16_t buffer[1024]={0};
uint8_t buffer_num=0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM6_Init(void);
/* USER CODE BEGIN PFP */
void Read_input();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_1,(uint32_t*) buffer, 1024, DAC_ALIGN_12B_R);
	wave_fillbuffer(buffer, buffer_num, 512);
}

void HAL_DAC_ConvHalfCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	wave_fillbuffer(buffer+512, buffer_num, 512);
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */
	wave_init();
	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART2_UART_Init();
	MX_DAC_Init();
	MX_TIM2_Init();
	MX_TIM6_Init();
	/* USER CODE BEGIN 2 */
	__HAL_TIM_ENABLE(&htim2);
	__HAL_TIM_ENABLE(&htim6);
	HAL_UART_Transmit(&huart2, myStudentNumber, 10, 100);


	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		uint8_t debounce = 0;
		uint8_t RecStart = 0;
		uint8_t PlayStart = 0;
		uint8_t RecSlot = 0;
		uint32_t ClockCount = 0;
		uint32_t ClockCount2 = 0;

		uint32_t Time =  HAL_GetTick();
		uint32_t TimeCount = (Time - ClockCount);

		if (interupt && !debounce){// debounce check
			debounce = 1;
			ClockCount = Time;
		}

		if (debounce && (TimeCount >= 10)){
			debounce = 0;
			interupt = 0;
			Read_input();

		}

		if (but_rec && (but_1 || but_2 || but_3)){ //record check
			while (but_1 || but_2 || but_3){
				RecSlot = but_1 + 2*but_2 + 3*but_3;
				Read_input();
			}
			RecStart = 1;
		}

		if (!but_rec && (but_1 || but_2 || but_3)){//play check
			while (but_1 || but_2 || but_3){
				RecSlot = but_1 + 2*but_2 + 3*but_3;
				Read_input();
			}
			PlayStart = 1;
		}

		if (RecStart){ //record loop
			RecStart = 0;
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1);
			Record[9] = RecSlot + 48;
			HAL_UART_Transmit(&huart2, Record, 10, 100);
			Time = HAL_GetTick();
			ClockCount = Time;
			TimeCount = (Time - ClockCount);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
			uint32_t RecordingTime = Time;

			while (!but_stop && ((Time - RecordingTime) <= 20000)){ //check record time
				Time = HAL_GetTick();
				TimeCount = (Time - ClockCount);
				if (TimeCount >= 250){
					ClockCount = Time;
					switch (RecSlot) {
					case 1:
						HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);
						break;
					case 2:
						HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
						break;
					case 3:
						HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);
						break;
					default:
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
						HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
						break;
					}
				}

				uint32_t Time2 =  HAL_GetTick();
				uint32_t TimeCount2 = (Time2 - ClockCount2);
				if (interupt && !debounce){
					debounce = 1;
					ClockCount2 = Time2;
				}

				if (debounce && (TimeCount2 >= 10)){
					debounce = 0;
					interupt = 0;
					Read_input();
				}

				if (but_stop || ((Time2 - RecordingTime) > 20000)){//check stop record
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
					HAL_UART_Transmit(&huart2, Stop, 10, 100);
				}


			}
		}

		if (PlayStart){ //play loop
			PlayStart = 0;
			Play[9] = RecSlot + 48;
			HAL_UART_Transmit(&huart2, Play, 10, 100);
			switch(RecSlot){
			case 1:
				buffer_num=1;
				break;
			case 2:
				buffer_num=2;
				break;
			case 3:
				buffer_num=3;
				break;
			}
			wave_fillbuffer(buffer, buffer_num, 1024);
			HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) buffer, 1024, DAC_ALIGN_12B_R);
			Time = HAL_GetTick();
			ClockCount = Time;
			TimeCount = (Time - ClockCount);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
			uint32_t PlayCount = Time;
			while ((!but_stop) && ((Time - PlayCount) <= 20000)){
				Time = HAL_GetTick();
				TimeCount = (Time - ClockCount);
				if (TimeCount >= 250){
					ClockCount = Time;
					switch (RecSlot) {
					case 1:
						HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);
						break;
					case 2:
						HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
						break;
					case 3:
						HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);
						break;
					default:
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, 0);
						HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
						break;
					}
				}

				uint32_t Time2 =  HAL_GetTick();
				uint32_t TimeCount2 = (Time2 - ClockCount2);
				if (interupt && !debounce){
					debounce = 1;
					ClockCount2 = Time2;
				}

				if (debounce && (TimeCount2 >= 10)){
					debounce = 0;
					interupt = 0;
					Read_input();
				}

				if ((but_stop) || ((Time - PlayCount) > 20000)){
					RecStart = 0;
					PlayStart= 0;
					RecSlot = 0;
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
					HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
					HAL_UART_Transmit(&huart2, Stop, 10, 100);


				}
			}
		}


		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}

	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 16;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ = 2;
	RCC_OscInitStruct.PLL.PLLR = 2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
 * @brief DAC Initialization Function
 * @param None
 * @retval None
 */
static void MX_DAC_Init(void)
{

	/* USER CODE BEGIN DAC_Init 0 */

	/* USER CODE END DAC_Init 0 */

	DAC_ChannelConfTypeDef sConfig = {0};

	/* USER CODE BEGIN DAC_Init 1 */

	/* USER CODE END DAC_Init 1 */
	/** DAC Initialization
	 */
	hdac.Instance = DAC;
	if (HAL_DAC_Init(&hdac) != HAL_OK)
	{
		Error_Handler();
	}
	/** DAC channel OUT1 config
	 */
	sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN DAC_Init 2 */

	/* USER CODE END DAC_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void)
{

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	TIM_MasterConfigTypeDef sMasterConfig = {0};

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 0;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 0;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
	{
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */

	/* USER CODE END TIM2_Init 2 */

}

/**
 * @brief TIM6 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM6_Init(void)
{

	/* USER CODE BEGIN TIM6_Init 0 */

	/* USER CODE END TIM6_Init 0 */

	TIM_MasterConfigTypeDef sMasterConfig = {0};

	/* USER CODE BEGIN TIM6_Init 1 */

	/* USER CODE END TIM6_Init 1 */
	htim6.Instance = TIM6;
	htim6.Init.Prescaler = 0;
	htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim6.Init.Period = 1904;
	htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
	{
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN TIM6_Init 2 */

	/* USER CODE END TIM6_Init 2 */

}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void)
{

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 500000;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/** 
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) 
{

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Stream5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, LD2_Pin|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_9, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : LD2_Pin PA6 PA7 PA9 */
	GPIO_InitStruct.Pin = LD2_Pin|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : PC7 */
	GPIO_InitStruct.Pin = GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : Record_Pin */
	GPIO_InitStruct.Pin = Record_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(Record_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PB4 PB8 PB9 */
	GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_8|GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : Stop_Pin */
	GPIO_InitStruct.Pin = Stop_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(Stop_GPIO_Port, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
/*void HAL_GPIO_EXTI_IRQHandler(uint16_t GPIO_Pin){

Button3 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4);
Button2 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5);
Button1 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8);
ButtonStop = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9);
ButtonRecord = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_10);
}*/
/* USER CODE BEGIN 4 */
void Read_input(){
	but_1 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4);
	but_2 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8);
	but_3 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9);
	but_rec = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_10);
	but_stop = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5);
}



/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
